from Inquisitor.plugin import Plugin
from Inquisitor.plugin import InvalidCommandException

import discord


class RulesCommandException(InvalidCommandException):
    def __init__(self):
        InvalidCommandException.__init__(self, ";rules <NUMBER>")


class Rules(Plugin):
    message = None
    embed = None

    def __init__(self, client, settings):
        Plugin.__init__(self, 'ImBetterThan', client, settings)

    async def on_ready(self):
        message_id = int(self.settings['rule_message_id'])

        for channel in self.client.get_all_channels():
            try:
                message = await channel.fetch_message(message_id)
                break
            except:
                continue

        self.embed = discord.Embed(type="rich", description=message.content)
        self.embed.set_author(name="Règles")

    async def on_message(self, message):
        try:
            if message.content.startswith(";rules "):
                content = message.content.split()

                if len(content) == 1:
                    await message.channel.send(embed=self.embed)
                elif len(content) == 2:
                    rule_id = 1
                    try:
                        rule_id = int(content[1]) - 1
                    except ValueError:
                        raise RulesCommandException

                    if rule_id > len(self.settings['rules']):
                        await message.channel.send("Unknown rule")
                    else:
                        await message.channel.send(self.settings['rules'][rule_id])
                else:
                    raise RulesCommandException
        except PluginException as err:
            await message.channel.send(err.message)
