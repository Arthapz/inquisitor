from Inquisitor.plugin import Plugin
from Inquisitor.plugin import PluginException
from Inquisitor.plugin import InvalidCommandException

import discord

HELP_MESSAGE = ";quote <MESSAGE_URI> # quote and save a message"

class MessageNotFoundException(PluginException):
    def __init__(self):
        PluginException.__init__(self, "Message not found")


class QuoteCommandException(InvalidCommandException):
    def __init__(self):
        InvalidCommandException.__init__(self, HELP_MESSAGE)


class Quote(Plugin):
    def __init__(self, client, settings):
        Plugin.__init__(self, 'Quote', client, settings)

    async def on_message(self, message):
        content = message.content.split()

        if  not len(content) > 0 \
        or not content[0] == ";quote":
            return

        if len(content) == 1:
           raise QuoteCommandException()

        if content[1] == "help":
            await self.help(message)

    async def help(self, message):
        await message.channel.send(HELP_MESSAGE)

    async def quote(self, message_to_quote):
        name = message_to_quote.author.name

        embed = discord.Embed(type = "rich", description = message_to_quote.content, colour = message_to_quote.author.color)
        embed.set_author(name = name, icon_url = message_to_quote.author.avatar_url)
        embed.set_footer(text = "quoted from: {}\ncreated at {}\nrequested by {}".format(message.id, message.created_at, message.author.nick))

        await message.channel.send(embed = embed)
