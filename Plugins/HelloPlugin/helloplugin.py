from Inquisitor.plugin import Plugin


class HelloPlugin(Plugin):
    channel_id = None
    channel = None

    def __init__(self, client, settings):
        Plugin.__init__(self, "HelloPlugin", client, settings)

        self.channel_id = int(self.settings["channel_id"])

    async def on_ready(self):
        self.channel = self.client.get_channel(self.channel_id)
        await self.channel.send("-- 🤖 Inquisitor V2.0 initialized 🤖 --")
