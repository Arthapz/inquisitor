from Plugins.HelloPlugin.helloplugin import HelloPlugin


def get_plugin(client, settings):
    return HelloPlugin(client, settings)
