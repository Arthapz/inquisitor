from setuptools import setup

setup(
   name='Inquisitor',
   version='1.0',
   description='Modular discord bot',
   author='Arthur LAURENT',
   author_email='arthur.laurent4@gmail.com',
   packages=['Inquisitor'],  # same as name
   install_requires=['discord.py'],  # external packages as dependencies
)
