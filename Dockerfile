FROM python:3

COPY . /inquisitor
WORKDIR /inquisitor

RUN pip install -r requirements.txt

CMD [ "python", "-m", "Inquisitor"]