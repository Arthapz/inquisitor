class PluginException(Exception):
    message = None

    def __init__(self, message):
        self.message = message


class InvalidCommandException(PluginException):
    def __init__(self, message):
        PluginException.__init__(self, "Invalid command usage, correct usage: {}".format(message))


class Plugin():
    settings = None
    client = None
    name = ""

    def __init__(self, name, client, settings):
        self.name = name
        self.settings = settings
        self.client = client

    def name(self):
        return self.name

    async def on_ready(self):
        pass

    async def on_message(self, message):
        pass
