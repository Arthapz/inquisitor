import json
import discord
import imp
import os


PLUGIN_FOLDER = "Plugins"
MAIN_MODULE = "__init__"


class Bot():
    settings = None
    client = None
    plugins = {}

    def __init__(self):
        json_data = ""

        with open("settings.json", encoding='utf8') as file:
            json_data = file.read()

        self.settings = json.loads(json_data)

        self.client = discord.Client()

    def start(self):
        self.client.on_ready = self.on_ready
        self.client.on_message = self.on_message
        self.client.run(self.settings['token'])

    async def on_ready(self):
        print('[Info] We have logged in as {0.user}'.format(self.client))

        for plugin in self.settings["enabled_plugins"]:
            plugin_path = os.path.join(PLUGIN_FOLDER, plugin)

            if not os.path.isdir(plugin_path) \
               or not MAIN_MODULE + ".py" in os.listdir(plugin_path):
                print("[Error] Failed to find {} in plugin ({}) directory".format(plugin, PLUGIN_FOLDER))
                continue

            info = imp.find_module(MAIN_MODULE, [plugin_path])

            module = imp.load_module(MAIN_MODULE, *info)

            settings = {}
            if plugin in self.settings:
                settings = self.settings[plugin]

            self.plugins[plugin] = module.get_plugin(self.client, settings)

            print("[Info] {} plugin loaded".format(plugin))

        for name, plugin in self.plugins.items():
            await plugin.on_ready()
            print("[Info] {} plugin initialized".format(name))

    async def on_message(self, message):
        for name, plugin in self.plugins.items():
            await plugin.on_message(message)
