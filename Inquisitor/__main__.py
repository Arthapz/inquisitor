from Inquisitor.bot import Bot


def main():
    inquisitor = Bot()
    inquisitor.start()


if __name__ == "__main__":
    main()
